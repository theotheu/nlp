#!/bin/bash

echo "##### Creating config file"
echo '"baseUrl: "https://theotheu.gitlab.io/nlp/"
languageCode: "en-US"
defaultContentLanguage: "en"

title: "My Project Docs Site"
theme: "learn"
metaDataFormat: "yaml"
defaultContentLanguageInSubdir: true

params:
  editURL: "https://gitlab.com/theotheu/nlp/tree/master/docs/content/"
  description: "Description of project docs site"
  author: "Consensus Enterprises"
  showVisitedLinks: true
  disableBreadcrumb: false
  disableNextPrev: false
  disableSearch: false
  disableAssetsBusting: false
  disableInlineCopyToClipBoard: false
  disableShortcutsTitle: false
  disableLanguageSwitchingButton: false
  ordersectionsby: "weight" # or "title"

menu:
  shortcuts:
    - name: "<i class='fa fa-gitlab'>.</i> Gitlab repo"
      url: "https://gitlab.com/theotheu/nlp"
      weight: 10
    - name: "<i class='fa fa-bullhorn'>.</i> Contributors"
      url: "https://gitlab.com/theotheu/nlp/graphs/master"
      weight: 30

# For search functionality
outputs:
  home:
    - "HTML"
    - "RSS"
    - "JSON"' > wegermee.yml
