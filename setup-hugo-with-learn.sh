#!/bin/bash
export PROJECT_ROOT=`pwd`
export SITE=site

rm -fr $SITE
rm -fr .hugo_build-lock*
cd .git


echo "##### Cleaning up in the .git directory"
rm -f index
rm -fr ./modules/*
find ./ -type f -exec sed -i '' "s/\[submodule "hugo-theme-learn"\]//g" {} +
find ./ -type f -exec sed -i '' "s/\[submodule "learn"\]//g" {} +
find ./ -type f -exec sed -i '' "s/hugo\/learn experiment//g" {} +

cd -
git reset
echo "##### Creating new hugo site"
cd ${PROJECT_ROOT}
hugo new site ${SITE}

echo "--- `pwd`"


echo "##### Adding theme"
git submodule add https://github.com/matcornic/hugo-theme-learn.git ${SITE}/themes/learn

echo "##### Adding ${SITE} to git"
git add ${SITE}

echo "##### Creating config file"
cd ${PROJECT_ROOT}/${SITE}
echo "----- `pwd`"
echo 'baseUrl: "https://theotheu.gitlab.io/nlp/"
languageCode: "en-US"
defaultContentLanguage: "en"

title: "My Project Docs Site"
theme: "learn"
metaDataFormat: "yaml"
defaultContentLanguageInSubdir: true

params:
  editURL: "https://gitlab.com/theotheu/nlp/tree/master/docs/static/"
  description: "Description of project docs site"
  author: "Consensus Enterprises"
  showVisitedLinks: true
  disableBreadcrumb: false
  disableNextPrev: false
  disableSearch: false
  disableAssetsBusting: false
  disableInlineCopyToClipBoard: false
  disableShortcutsTitle: false
  disableLanguageSwitchingButton: false
  ordersectionsby: "weight" # or "title"

menu:
  shortcuts:
    - name: "Gitlab repo"
      url: "https://gitlab.com/theotheu/nlp"
      weight: 10
    - name: "Contributors"
      url: "https://gitlab.com/theotheu/nlp/graphs/master"
      weight: 30

# For search functionality
outputs:
  home:
    - "HTML"
    - "RSS"
    - "JSON"' > config.yml

rm -f ${PROJECT_ROOT}/${SITE}/config.toml

echo "##### Adding Search"
cd ${PROJECT_ROOT}/${SITE}/layouts
echo "----- `pwd`"

echo '[{{ range $index, $page := .Site.Pages }}
{{- if ne $page.Type "json" -}}
{{- if and $index (gt $index 0) -}},{{- end }}
{
        "uri": "{{ $page.Permalink }}",
        "title": "{{ htmlEscape $page.Title}}",
        "tags": [{{ range $tindex, $tag := $page.Params.tags }}{{ if $tindex }}, {{ end }}"{{ $tag| htmlEscape }}"{{ end }}],
        "description": "{{ htmlEscape .Description}}",
        "content": {{$page.Plain | jsonify}}
}
{{- end -}}
{{- end -}}]' > index.json

echo
echo "##### Change directory to ${SITE} and enter 'hugo serve'"
echo
exit
