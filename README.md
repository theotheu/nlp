Create new data sets on http://node158.tezzt.nl/novel_approaches/

# Jekyll
View documentation locally with jekyll by running
`bundle exec jekyll serve`

# Hugo (option 1)
1. `npm create quickstart`
2. `cd new-project`
3. `npm run start:all`

# Hugo (option 2)
1. hugo new site site


# Learn
- cd ./site/themes/
- git clone https://github.com/matcornic/hugo-theme-learn.git
- git submodule add https://github.com/matcornic/hugo-theme-learn.git
- hugo new posts/my-first-post.md

This one works: https://consensus.enterprises/blog/add-hugo-docs-to-gitlab-project/