#!/bin/bash

echo "Killing shell processes..."
kill -9 `ps -aef|grep -v grep|grep 0.process_repos.sh |awk {'print $2'}` 2>/dev/null
kill -9 `ps -aef|grep -v grep|grep keep_running.sh |awk {'print $2'}` 2>/dev/null

echo "Killing python processes..."
kill -9 `ps -aef|grep -v grep|grep -v root|grep -v PyCharm|grep -v Dropbox|grep python|awk {'print $2'}` 2>/dev/null

kill -9 `ps -aef|grep -v grep|grep 1b.run_all_processes.sh|awk {'print $2'}` 2>/dev/null
