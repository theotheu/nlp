import mysql.connector
import dbconfig as cfg

import nltk
import re
import hashlib

import signal
import sys
import ssl

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context

nltk.download("punkt")
nltk.download('averaged_perceptron_tagger')
from nltk.tree import Tree
from nltk.draw.tree import TreeView
from PIL import Image

# @see https://github.com/nltk/nltk/issues/2812
from nltk.parse import CoreNLPParser

parser = CoreNLPParser()

s = "Barack Obama was born in Hawaii."
s = "They cannot go to cinema because it is too late."
s = "Because the alarm was not set, we were late for work."
s = "They cannot go to cinema because it is too late. A tornado blew the roof off the house, and as a result, the family had to find another place to live."
s = "Zij is vandaag jarig"

import stanza

stanza.download('en')  # download English model
nlp = stanza.Pipeline('en')  # initialize English neural pipeline


# nlp = stanza.Pipeline(lang='en', processors='tokenize,pos,constituency', package={'constituency': 'wsj_bert'})

def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        host = cfg.mysql["host"],
        user = cfg.mysql["user"],
        password = cfg.mysql["password"],
        database = cfg.mysql["db"],
        # auth_plugin = 'mysql_native_password'
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            buffered=True
        )
    return mydb


def traverse_tree(keywords, tree):
    global p
    try:
        for sub_tree in tree:
            if type(sub_tree) == nltk.tree.Tree:
                for keyword in keywords:
                    if keyword in str(sub_tree).lower():
                        h = sub_tree.height()
                        # print('-----', h, sub_tree)
                        tree_labels = Tree.fromstring(str(sub_tree),
                                                      read_node=lambda s: '%s' % s,
                                                      read_leaf=lambda s: '')
                        tree_labels = str(tree_labels)
                        tree_labels = re.sub('\s{2,}', ' ', tree_labels)
                        tree_labels = re.sub('\n', '', tree_labels)
                        p.append([h, tree_labels])
                traverse_tree(keywords, sub_tree)
    except:
        print("Probably recursion to deep. ", tree)


p = []
ss = []


def save(mycursor, repos_meta_id, ss):
    for s in ss:
        for row in s:
            h = row[0]
            c = row[1]
            md5 = hashlib.md5(c.encode('utf-8')).hexdigest()
            p = c.split(' ')
            first_node = p[0].replace('(', '').strip()

            query = """
                       select `id` from clauses where `md5`=%s and height=%s
                       """
            data = (md5, h,)
            mycursor.execute(query, data)
            myresult = mycursor.fetchall()
            count = mycursor.rowcount
            if count == 0:
                # Create tree record
                query = "insert ignore into clauses (md5, height, first_node, clause, creation_date) values (%s, %s, %s, %s, now())"
                data = (md5, h, first_node, c)
                mycursor.execute(query, data)
                mydb.commit()
                clauses_id = mycursor.lastrowid
            else:
                clauses_id = myresult[0][0]

            query = """
                       select `id` from comments_with_clauses where repos_meta_id=%s and clauses_id=%s
                       """
            data = (repos_meta_id, clauses_id,)
            # print(">>>>> query: ", query)
            # print(">>>>> data: ", data)
            mycursor.execute(query, data)
            count = mycursor.rowcount
            # print(">>>>> count: ", count)
            if count == 0:
                # Create tree record
                query = "insert ignore into comments_with_clauses (repos_meta_id, clauses_id, creation_date) values (%s, %s, now())"
                data = (repos_meta_id, clauses_id,)
                mycursor.execute(query, data)
                mydb.commit()


def parse_comments(mycursor, keywords, repos_meta_id, sentences):
    global ss
    global p
    for sentence in sentences:
        tree = Tree.fromstring(str(sentence.constituency))
        p = []
        traverse_tree(keywords, tree)
        ss.append(p)
    save(mycursor, repos_meta_id, ss)
    ss = []


def get_keywords(mydb):
    keywords = []
    mycursor = mydb.cursor()
    query = """
select t.pattern
from search_terms t
where  t.relevance>%s
order by t.pattern
        """
    data = (.8,)
    print('>>>>> query: ', query)
    print('>>>>> data : ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()

    for r in myresult:
        keywords.append(r[0])

    return keywords


def clean_up_comment(subject, comment):
    comment = comment.replace(subject, '', 1).strip()
    comment = comment.replace('\n', ' ')
    sentences = comment.split('. ')
    return (sentences)


def get_starting_meta_repos_id(mycursor):
    query = """
    select repos_meta_id
from comments_with_clauses
order by id desc
limit 1;
"""
    data = ()
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    repos_meta_id = myresult[0][0]
    return repos_meta_id


def get_data(mydb):
    mycursor = mydb.cursor()
    repos_meta_id = get_starting_meta_repos_id(mycursor)
    print('repos_meta_id: ', repos_meta_id)
    query = """
select count(1) cnt, c.id repos_meta_id, c.subject, c.comment
from search_terms t
, search_results r
, repos_meta c
where
    c.id>%s
and c.id>11078391    
and t.relevance>%s
and length(c.comment)<1000*1024
and t.id=r.search_terms_id
and c.id=r.repos_meta_id
group by c.id
order by c.id
limit %s
    """
    data = (repos_meta_id, 0.8, 1000,)
    print('>>>>> query: ', query)
    print('>>>>> data : ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    return myresult


def parse_data(mydb, data):
    mycursor = mydb.cursor()

    keywords = get_keywords(mydb)
    i = 0

    for r in data:
        repos_meta_id = r[1]
        subject = r[2]
        comment = r[3]

        print('Processing repos_meta_id: ', i, repos_meta_id, len(comment))
        i += 1

        cleaned_comments = clean_up_comment(subject, comment)
        for comment in cleaned_comments:
            doc = nlp(comment)
            parse_comments(mycursor, keywords, repos_meta_id, doc.sentences)

    mycursor.close()

# data=[[1, 0, '', s]]
mydb = create_db_connection()
data = get_data(mydb)
parse_data(mydb, data)
# print (ss)