import pandas as pd
from datetime import datetime
import numpy as np
import mysql.connector
import sys
import csv
import os
import argparse
import time
import dbconfig as cfg

start_time = datetime.now()
minimal_word_length = 2
# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')

args = parser.parse_args()

repo_name = args.repository

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR = '/'.join(parts)
SOURCE_DIR = BASE_DIR + '/repositories/' + repo_name
print('SOURCE_DIR   :', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print('ANALYSIS_DIR :', ANALYSIS_DIR)


def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            auth_plugin='mysql_native_password',
            autocommit=True
        )
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            auth_plugin='mysql_native_password',
            autocommit=True
        )

    return mydb


def get_repos_id(mycursor, repo_name):
    query = 'select id from repos where name=%s'
    data = (repo_name,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    repos_id = myresult[0][0]
    return repos_id


def run_query_in_parts(mycursor, hashes):
    chunk_size = 100
    l = len(hashes)
    l_counter = 0
    joined_results = []
    while l_counter < l:
        some_hashes = hashes[l_counter:l_counter + chunk_size]

        query = 'select UNIX_TIMESTAMP(comitterDate) comitterDate, comment from repos_meta where hash in ("'
        query += '","'.join(some_hashes)
        query += '") '
        # query += 'order by comitterDate'
        data = ()
        # print('>>>>> query: ', query)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        joined_results = [*joined_results, *myresult]

        l_counter += chunk_size

    joined_results.sort(key=lambda tup: tup[0])

    result = [a_tuple[1] for a_tuple in joined_results]

    comments = ' '.join(result)
    comments = comments.replace('\n', ' ')

    return comments


def check_if_file_is_unzipped(filename):
    zipped_filename = filename + '.gz'
    print('\n>>>>> Checking zipped file...')
    if os.path.isfile(zipped_filename):
        print('>>>>> Gzipped file exist. Now unzipping...')
        cmd = 'gunzip ' + zipped_filename
        stream = os.popen(cmd)
        output = stream.read()
        print('>>>>> output', output)
        print('>>>>> Done unzipping.')
    else:
        print('>>>>> Already unzipped.')


def prepare_session(mycursor):
    # https://dba.stackexchange.com/questions/280163/mysql-event-scheduler-waiting-on-empty-queue-since-server-restarted-12-days-ago
    query = 'SET @@global.event_scheduler=0'
    print('Preparing session: ', query)
    mycursor.execute(query)

    # https://scalegrid.io/blog/calculating-innodb-buffer-pool-size-for-your-mysql-server/
    size = str(256 * 1024 * 1024 + 256 * 4)
    query = 'SET GLOBAL innodb_buffer_pool_size=' + size
    print('Preparing session: ', query)
    mycursor.execute(query)

    # default 50
    query = 'SET GLOBAL innodb_lock_wait_timeout=300'
    print('Preparing session: ', query)
    mycursor.execute(query)

    # https://stackoverflow.com/questions/5836623/getting-lock-wait-timeout-exceeded-try-restarting-transaction-even-though-im
    # https://dev.mysql.com/doc/refman/5.7/en/innodb-transaction-isolation-levels.html#isolevel_repeatable-read
    query = "SET transaction_isolation = 'READ-COMMITTED'"
    print('Preparing session: ', query)
    mycursor.execute(query)

    # https://stackoverflow.com/questions/5836623/getting-lock-wait-timeout-exceeded-try-restarting-transaction-even-though-im
    # https://dev.mysql.com/doc/refman/5.7/en/innodb-transaction-isolation-levels.html#isolevel_repeatable-read
    query = "SET GLOBAL transaction_isolation = 'READ-COMMITTED'"
    print('Preparing session: ', query)
    mycursor.execute(query)


def delete_in_parts(mycursor, repos_id):
    chunk = 10000

    query = 'select count(1) cnt from tfidf_for_search_terms where repos_id =%s'
    data = (repos_id,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    cnt = myresult[0][0]

    records_to_remove = cnt
    while records_to_remove > 0:
        print('Records to remove for repos_id=', repos_id, ' is ', '{:,.0f}'.format(records_to_remove)
          , datetime.now())
        query = 'delete from tfidf_for_search_terms where repos_id=%s limit %s'
        data = (repos_id, chunk)
        print(query)
        print(data)
        mycursor.execute(query, data)
        records_to_remove -= chunk

    # Rebuild index

#    query = 'OPTIMIZE TABLE tfidf_for_search_terms'
#    print ('Rebuilding index (and stats)...', start_time.now())
#    data = ()
#    print(query)
#    print(data)
#    mycursor.execute(query, data)
    print ('Done', start_time.now())


def read_file_from_disk(in_filename):
    global minimal_word_length

    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)

    prepare_session(mycursor)

    repos_id = get_repos_id(mycursor, repo_name)

    print('>>>>> ANALYSIS_DIR: ', ANALYSIS_DIR)
    print('>>>>> Processing repository: ', repo_name)
    print('>>>>> in_filename          : ', in_filename)
    print('>>>>> repos_id             : ', repos_id)

    check_if_file_is_unzipped(in_filename)

    with open(in_filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        records_to_insert = []
        seq = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
                delete_start_time = datetime.now()
                print('Start deletion at ', datetime.now(), )
                delete_in_parts(mycursor, repos_id)
                print('Finished deletion at ', datetime.now(), datetime.now() - delete_start_time)

                # mydb.commit()
            elif len(row) == 8 and len(row[1]) >= minimal_word_length:
                repos_meta_id = str(row[0])
                word = str(row[1])
                ftd = str(row[2])
                tf = str(row[3])
                nt = str(row[4])
                nnt = str(row[5])
                idf = str(row[6])
                tfidf = str(row[7])

                query = 'insert into tfidf_for_search_terms (seq, repos_id, repos_meta_id, word, ftd, tf, nt, nnt, idf, tfidf) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                data = (seq, repos_id, repos_meta_id, word, ftd, tf, nt, nnt, idf, tfidf)
                records_to_insert.append((data))
                seq += 1
                # print(query)
                # print(data)

                if line_count % 10000 == 0:
                    print('\n..... Still processing : ', repo_name)
                    print('            Line count : ', '{:,.0f}'.format(line_count))
                    # print ('                 Query : ', query)
                    # print ('             repos_id  : ', '{:,.0f}'.format(repos_id))
                    # print ('        repos_meta_id  : ', '{:,.0f}'.format(int(row[0])))
                    # print ('                 word  : ', row[1])
                    # print ('                  ftd  : ', '{:,.2f}'.format(float(row[2])))
                    # print ('                   tf  : ', '{:,.0f}'.format(float(row[3])))
                    # print ('                   nt  : ', '{:,.0f}'.format(float(row[4])))
                    # print ('                  nnt  : ', '{:,.2f}'.format(float(row[5])))
                    # print ('                  idf  : ', '{:,.2f}'.format(float(row[6])))
                    # print ('                tfidf  : ', '{:,.4f}'.format(float(row[7])))
                    print('             Duration  : ', datetime.now() - start_time)

                if len(records_to_insert) > 1000:
                    try:
                        mycursor.executemany(query, records_to_insert)
                        records_to_insert = []
                        time.sleep(10)
                    except mysql.connector.Error as err:
                        print("!!! Something went wrong: {}".format(err))
                        print ("Error code:", err.errno)  # error number
                        print ("SQLSTATE value:", err.sqlstate)  # SQLSTATE value
                        print ("Error message:", err.msg)  # error message
                        print ("Error:", err)  # errno, sqlstate, msg values
                        s = str(err)
                        print ("Error:", s)  # errno, sqlstate, msg values
                        print ('err: ', err)
                        print(query)
                        print('data           : ', data[0])
                        print('# data elements: ', len(data))
                        sys.exit()

                line_count += 1
        print(f'Processed {line_count} lines.')


# def ___read_file_from_disk(dir, in_filename):
#     mydb = create_db_connection()
#     mycursor = mydb.cursor()
#     repos_id = get_repos_id(mycursor, repo_name)
#     print('\n------------------------------------------')
#     print('>>>>> ANALYSIS_DIR: ', ANALYSIS_DIR)
#     print('>>>>> Processing repository: ', repo_name)
#     print('>>>>> in_filename          : ', in_filename)
#     print('>>>>> repos_id             : ', repos_id)
#     df = pd.read_csv(in_filename, delimiter=';', quotechar='"', header=0, engine='python')
#     number_of_work_items = df.shape[0]
#     print('>>>>> shape: ', number_of_work_items)
#
#     t = datetime.now()
#
#     for index, row in df.iterrows():
#         repos_meta_id = str(row[0])
#         word = str(row[1])
#         ftd = str(row[3])
#         tf = str(row[3])
#         nt = str(row[4])
#         nnt = str(row[5])
#         idf = str(row[6])
#         tfidf = str(row[7])
#
#         if index == 0:
#             query = 'delete from tfidf_for_search_terms where repos_id=%s'
#             data = (repos_id,)
#             print(query)
#             print(data)
#             mycursor.execute(query, data)
#             mydb.commit()
#             seq = 0
#
#         # query = 'select max(seq)+1 seq where repos_id=%s'
#         # data = (repos_id,)
#         # mycursor.execute(query, data)
#         # myresult = mycursor.fetchall()
#         # seq = myresult[0][0]
#         seq += 1
#
#         query = 'insert into tfidf_for_search_terms (seq, repos_id, repos_meta_id, search_terms_id, ftd, tf, nt, nnt, idf, tfidf) values %s, (%s, %s, %s, %s, %s, %s, %s, %s, %s)'
#         data = (seq, repos_id, repos_meta_id, word, ftd, tf, nt, nnt, idf, tfidf)
#         # print(query)
#         # print(data)
#         try:
#             mycursor.execute(query, data)
#             mydb.commit()
#         except:
#             print('!!!')
#             print('index=', index)
#             print('seq=', seq)
#             print(query)
#             print(data)
#             # sys.exit()
#
#     mycursor.close()
#     mydb.close()


dir = ANALYSIS_DIR
in_filename = dir + '/A2.tf-idf_from_search_terms.csv'

read_file_from_disk(in_filename)

print('Gzip file...')

cmd = 'gzip ' + in_filename
stream = os.popen(cmd)
output = stream.read()
print('output', output)

print('DONE!', datetime.now() - start_time)
