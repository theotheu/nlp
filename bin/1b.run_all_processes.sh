#!/bin/bash

./0.process_repos.sh -r chaosmonkey -s A1,A2,A3
./0.process_repos.sh -r bash -s A1,A2,A3
./0.process_repos.sh -r latex2e -s A1,A2,A3
./0.process_repos.sh -r express -s A1,A2,A3
./0.process_repos.sh -r nginx -s A1,A2,A3
./0.process_repos.sh -r che -s A1,A2,A3
./0.process_repos.sh -r notebook -s A1,A2,A3
./0.process_repos.sh -r react -s A1,A2,A3
./0.process_repos.sh -r flutter -s A1,A2,A3
./0.process_repos.sh -r cassandra -s A1,A2,A3
./0.process_repos.sh -r react-native -s A1,A2,A3
./0.process_repos.sh -r brew -s A1,A2,A3
./0.process_repos.sh -r sonarqube -s A1,A2,A3
./0.process_repos.sh -r jenkins -s A1,A2,A3
# ./0.process_repos.sh -r angular -s A1,A2,A3
./0.process_repos.sh -r bitcoin -s A1,A2,A3
./0.process_repos.sh -r atom -s A1,A2,A3
./0.process_repos.sh -r WordPress -s A1,A2,A3
./0.process_repos.sh -r ansible -s A1,A2,A3
./0.process_repos.sh -r git -s A1,A2,A3
./0.process_repos.sh -r jdk -s A1,A2,A3
./0.process_repos.sh -r neo4j -s A1,A2,A3
./0.process_repos.sh -r vscode -s A1,A2,A3
./0.process_repos.sh -r vlc -s A1,A2,A3
./0.process_repos.sh -r rails -s A1,A2,A3
./0.process_repos.sh -r v8 -s A1,A2,A3
./0.process_repos.sh -r mediawiki -s A1,A2,A3
# ./0.process_repos.sh -r kubernetes -s A1,A2,A3
# ./0.process_repos.sh -r elasticsearch -s A1,A2,A3
# ./0.process_repos.sh -r tensorflow -s A1,A2,A3
# ./0.process_repos.sh -r cpython -s A1,A2,A3
# ./0.process_repos.sh -r mysql-server -s A1,A2,A3
# ./0.process_repos.sh -r server -s A1,A2,A3
# ./0.process_repos.sh -r core -s A1,A2,A3
# ./0.process_repos.sh -r gecko-dev -s A1,A2,A3
# ./0.process_repos.sh -r linux -s A1,A2,A3
