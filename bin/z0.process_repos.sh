#!/bin/bash

user=theotheu
password=aQ743Gp-
host=localhost
database=new_approaches

CWD=`pwd`


step0() {
  WD=$1
  cd ${CWD}/../repositories/
  git clone $1 2>/dev/null
}

step1() {
  PWD=`pwd`
  WD=$1
  cd ${CWD}/../repositories/${WD}
  git fetch --all && git reset --hard && git pull
}

mysql --user=$user --password=$password --host=$host $database -e "SELECT id, name, url, display_name from repos where is_active=1 order by lower(name)" |
  while read id name url display_name; do
    echo
    echo
    echo
    echo "Processing: $display_name"
    step0 $url
    step1 $name

  done
