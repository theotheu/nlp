import sys
import mysql.connector
import dbconfig as cfg

filename = '_v8.A1.out'
repos_id = 37


def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        host = cfg.mysql["host"],
        user = cfg.mysql["user"],
        password = cfg.mysql["password"],
        database = cfg.mysql["db"],
        auth_plugin = 'mysql_native_password'
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            autocommit=True
        )
    return mydb


def get_data():
    with open(filename, 'r') as file:
        lines = file.read().splitlines()  # .replace('\n', '')

    data = []
    for line in lines:
        if 'Records_to_insert' in line:
            l = line.replace('Records_to_insert :  [', '').replace(']', '')
            l = str(l)
            ls = l.split('), (')
            for r in ls:
                r = r.replace('(', '').replace(')', '')
                data.append(r)
        # else:
        #     print (line)
    return data


def write_records_to_database(mycursor, records_to_insert):
    query = 'insert into search_results (repos_id, repos_meta_id, search_terms_id, patterns_found, stemmed_found, lemmatized_found) values (%s, %s, %s, %s, %s, %s)'
    print('                          Query : ', query)
    print('              Records_to_insert : ', len(records_to_insert))
    try:
        mycursor.executemany(query, records_to_insert)
    except mysql.connector.Error as err:
        print("Something went wrong: {}".format(err))
        print('!!!')
        print('!     query : ', query)
        print('! len(row)  : ', len(records_to_insert))
        print('! first row : ', records_to_insert[0])
        # sys.exit()
    return []


def insert_into_database(records):
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)

    query = 'delete from search_results where repos_id=%s'
    data = (repos_id,)
    print('Delete query: ', query)
    print('Delete data: ', data)
    mycursor.execute(query, data)

    records_to_insert = []
    for r in records:
        r = r.replace("'", '')
        t = tuple(r.split(', '))
        if len(records_to_insert) < 1000:
            records_to_insert.append(t)
        else:
            records_to_insert = write_records_to_database(mycursor, records_to_insert)
    write_records_to_database(mycursor, records_to_insert)
    mycursor.close()
    mydb.close()

records = get_data()
print(len(records))
print(records[0])
insert_into_database(records)
print ("Done!")
