#!/bin/bash

cd ../repositories

git clone https://github.com/angular/angular
git clone https://github.com/atom/atom
git clone https://github.com/MicrosoftDocs/azure-docs
git clone https://github.com/bminor/bash
git clone https://github.com/bitcoin/bitcoin
git clone https://github.com/Homebrew/brew
git clone https://github.com/apache/cassandra
git clone https://github.com/Netflix/chaosmonkey
git clone https://github.com/eclipse/che/
git clone https://github.com/chromium/chromium
git clone https://github.com/python/cpython
git clone https://github.com/expressjs/express
git clone https://github.com/flutter/flutter
git clone https://github.com/git/git
git clone https://github.com/jenkinsci/jenkins
git clone https://github.com/kubernetes/kubernetes
git clone https://github.com/latex3/latex2e
git clone https://github.com/mysql/mysql-server
git clone https://github.com/wikimedia/mediawiki
git clone https://github.com/nginx/nginx
git clone https://github.com/jupyter/notebook
git clone https://github.com/rails/rails
git clone https://github.com/facebook/react
git clone https://github.com/facebook/react-native
git clone https://github.com/SonarSource/sonarqube
git clone https://github.com/tensorflow/tensorflow
git clone https://github.com/microsoft/vscode
git clone https://github.com/WordPress/WordPress
