#!/usr/bin/env python

mysql = {
    "host": "HOSTNAME",
    "user": "USERNAME",
    "password": "PASSWORD",
    "db": "DATABASE",
    "version": 8
}
