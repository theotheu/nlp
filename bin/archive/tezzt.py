import os
import sys
import re
import csv
import pandas as pd
from datetime import datetime

start_time = datetime.now()

print(os.getcwd())

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

dirname = os.getcwd().split('/')
dirname.pop()
REPO_DIRECTORY = '/'.join(dirname) + '/repositories/'
ANALYSIS_DIRECTORY = '/'.join(dirname) + '/analysis/'
BASE_DIRECTORY = '/'.join(dirname)
os.chdir(REPO_DIRECTORY)

repo_dict = {}
repo_dict['WordPress'] = 14
repo_dict['angular'] = 8
repo_dict['ansible'] = 10
repo_dict['atom'] = 4
repo_dict['azure-docs'] = 5
repo_dict['bash'] = 9
repo_dict['bitcoin'] = 11
repo_dict['brew'] = 6
repo_dict['cassandra'] = 17
repo_dict['chaosmonkey'] = 32
repo_dict['che'] = 16
repo_dict['chromium'] = 12
repo_dict['cpython'] = 15
repo_dict['express'] = 3
repo_dict['flutter'] = 13
repo_dict['git'] = 24
repo_dict['jenkins'] = 33
repo_dict['kubernetes'] = 21
repo_dict['latex2e'] = 7
repo_dict['linux'] = 18
repo_dict['mediawiki'] = 23
repo_dict['nginx'] = 1
repo_dict['notebook'] = 20
repo_dict['rails'] = 25
repo_dict['react'] = 34
repo_dict['react-native'] = 2
repo_dict['sonarqube'] = 35
repo_dict['tensorflow'] = 19
repo_dict['vscode'] = 22

repos = [f.name for f in os.scandir() if f.is_dir() and f.name != 'analysis' and f.name != '.ipynb_checkpoints']

repos = ['bash']

repos.sort()


def get_branches():
    cmd = "git show-branch --topics --all"
    stream = os.popen(cmd)
    output = stream.read()
    branches = output.split('\n')
    branch_dict = {}
    for branch in branches:
        _branch = re.sub('^[^\[]+', '', branch)
        items = _branch.split(']')
        if len(items) > 1:
            branch_name = items[0]
            branch_name = branch_name.replace('[', '')
            branch_name = branch_name.replace(']', '')
            branch_description = items[1]
            branch_dict[branch_name] = branch_description
    return branch_dict


for repo in repos:
    print('STARTED PROCESSING: ', repo, datetime.now())
    data = []
    repo_id = repo_dict[repo]

    os.chdir(REPO_DIRECTORY + '/' + repo)

    branch_dict = get_branches()
    print('Got branches: ', datetime.now() - start_time)

    print(len(branch_dict))

    for branch_name in branch_dict:
        branch_description = branch_dict[branch_name]
        cmd = 'git --no-pager log ' + branch_name + ' --first-parent --no-merges --oneline --pretty="%H"'
        stream = os.popen(cmd)
        output = stream.read()
        hashes = output.split('\n')
        hashes_count = len(hashes)
        hashes_counter = 1
        for hash in hashes:
            if (hashes_counter % 10000 == 0):
                print('Another round: ', repo, branch_name, hashes_counter, hashes_count, datetime.now() - start_time)
            hashes_counter += 1
            row = [repo_id, branch_name, hash]
            data.append(row)
        # print('Done hashes for', branch_name, datetime.now() - start_time)
    print('Done finding hashes with branches : ', repo, datetime.now() - start_time)
    print(len(data))
    df = pd.DataFrame(data, columns=['repo_id', 'BranchName', 'CommitHash'])
    print(df.head())
    analysis_dirname = ANALYSIS_DIRECTORY + repo
    filename = analysis_dirname + '/commits_and_branches.csv'
    df.to_csv(filename, index=False, sep=';', header=True)
    #
    filename = analysis_dirname + '/branch_names_and_branche_descriptions.csv'
    outfile = open(filename, 'w')
#    with open(filename, 'w') as csv_file:
        # writer = csv.writer(csv_file)
    outfile.write('BranchName;BranchDescription\n')
    for key, value in branch_dict.items():
        outfile.write(str(repo_id) +';"' + str(key) + '";"' + str(value.replace('"', "'")) + '"\n')

    print('Done: ', repo, datetime.now() - start_time)
    print('All done: ', datetime.now() - start_time)
