# https://towardsdatascience.com/installing-tensorflow-on-the-m1-mac-410bb36b776
# NOT https://developer.apple.com/metal/tensorflow-plugin/
# https://www.tensorflow.org/tutorials/load_data/text

#  conda env create --file=environment.yml --name tf_m1

#     $ conda activate tf_m1
#
# To deactivate an active environment, use
#
#     $ conda deactivate


print("\n\n\n")
print("================================================================================")
print("Start with: `conda env create --file=environment.yml --name tf_m1`")
print("Run `conda activate tf_m1` to activate on Mac ARM")
print("Run `conda deactivate` to deactivate on Mac ARM")
print("================================================================================")
print("\n\n\n")

import tensorflow as tf
from tensorflow.keras import utils
from tensorflow.keras import losses
from tensorflow.keras import preprocessing
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from tensorflow.keras import layers
import pathlib
import sys

limit = 10000
train_test_ration = 0.8  #
out_dir = '/Users/theotheu/Desktop/tf/'
out_dir = '/Volumes/1tb'

number_of_labels = 9

data_url = 'https://storage.googleapis.com/download.tensorflow.org/data/stack_overflow_16k.tar.gz'
# dataset_dir = utils.get_file(
#     origin=data_url,
#     untar=True,
#     cache_dir='stack_overflow',
#     cache_subdir='')
data_url = 'file://' + out_dir + '/tf.tar.gz'
print('>>>>> data_url: ', data_url)

dataset_dir = utils.get_file(  # conda version
    origin=data_url,
    untar=True,
    fname='tf_new_approaches',
    cache_subdir='')

parent_dir = pathlib.Path(dataset_dir).parent

list(parent_dir.iterdir())
print (list(parent_dir.iterdir()))
