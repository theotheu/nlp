# https://dev.mysql.com/doc/internals/en/full-text-search.html
#
# w = (log(dtf)+1)/sumdtf * U/(1+0.0115*U) * log((N-nf)/nf)
#
# dtf     is the number of times the term appears in the document
# sumdtf  is the sum of (log(dtf)+1)'s for all terms in the same document
# U       is the number of Unique terms in the document
# N       is the total number of documents
# nf      is the number of documents that contain the term
from sqlalchemy import create_engine
import pandas as pd
import re
import nltk
import math
import gc
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from datetime import datetime

nltk.download('stopwords')
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()

start_time0 = datetime.now()

limit = 10
document_counter = 0
unique_word_counter = 0
string_length_treshold = 2
stop_words = set(stopwords.words('english'))

documents = {}

words = {}
words['plain'] = {}
words['stemmed'] = {}


def parse_document(repos_meta_id, record):
    if (repos_meta_id % 1000 == 0):
        print("parsing record", repos_meta_id, datetime.now())
    if (repos_meta_id % 10000 == 0):
        print('gc before: ', gc.get_count())
        g = gc.collect()
        print('gc after : ', gc.get_count())
    for w in re.sub("[^a-zA-Z]", " ", record.lower()).split():
        # for w in re.sub("[\!\(\)\-\[\]\{\}\;\?\@\#\$%\:\'\"\\,\.\/^\&\*\_]", " ", document.lower()).split():
        if not w in stop_words and len(w) > string_length_treshold:
            s = stemmer.stem(w)
            l = wordnet_lemmatizer.lemmatize(w)

            if str(repos_meta_id) not in documents:
                documents[str(repos_meta_id)] = {}
                documents[str(repos_meta_id)]['plain'] = {}
                documents[str(repos_meta_id)]['stemmed'] = {}

            if w not in documents[str(repos_meta_id)]['plain']:
                documents[str(repos_meta_id)]['plain'][w] = 0
            documents[str(repos_meta_id)]['plain'][w] += 1

            if s not in documents[str(repos_meta_id)]['stemmed']:
                documents[str(repos_meta_id)]['stemmed'][s] = 0
            documents[str(repos_meta_id)]['stemmed'][s] += 1

def process_documents(result_set):
    for index, record in result_set.iterrows():
        parse_document(record[0], record[1])
    print('Calculating stats duration', datetime.now() - start_time0)


def get_documents():
    sqlEngine = create_engine('mysql+pymysql://theotheu:aQ743Gp-@node157.tezzt.nl/new_approaches', pool_recycle=3600)
    dbConnection = sqlEngine.connect()
    query = "select id, comment from repos_meta limit " + str(limit)
    print('query: ', query)
    result_set = pd.read_sql_query(query, dbConnection)
    pd.set_option('display.expand_frame_repr', False)
    dbConnection.close()
    print('Retrieving documents duration', datetime.now() - start_time0)
    return result_set


result_set = get_documents()
process_documents(result_set)

number_of_documents = len(result_set)
print('==== N (number of document): ', number_of_documents)


def export_U_the_number_of_Unique_terms_in_the_document():
    f = open("OUT_U_is_the_number_of_Unique_terms_in_the_document_PLAIN_STEMMED.csv", "w")
    for record in documents:
        l = '\t'.join([str(record), str(len(documents[record]['plain'])), str(len(documents[record]['stemmed']))])
        l += '\n'
        f.write(l)
    f.close()


def export_plain_dtf_is_the_number_of_times_the_term_appears_in_the_document():
    f = open("OUT_PLAIN_export_dtf_is_the_number_of_times_the_term_appears_in_the_document.csv", "w")
    f1 = open("OUT_PLAIN_export_sum_dtf_is_the_sum_of_(log(dtf)+1)s_for_all_terms_in_the_same_document.csv", "w")
    for record in documents:
        sumdtf = 0
        for w in documents[record]['plain']:
            dtf = documents[record]['plain'][w]
            sumdtf += math.log10(dtf + 1)
            l = '\t'.join([str(record), w, str(dtf)])
            l += '\n'
            f.write(l)
        l = '\t'.join([str(record), str(sumdtf)])
        l += '\n'
        f1.write(l)
    f.close()
    f1.close()


def export_stemmed_dtf_is_the_number_of_times_the_term_appears_in_the_document():
    f = open("OUT_STEMMED_export_dtf_is_the_number_of_times_the_term_appears_in_the_document.csv", "w")
    f1 = open("OUT_STEMMED_export_sum_dtf_is_the_sum_of_(log(dtf)+1)s_for_all_terms_in_the_same_document.csv", "w")
    for record in documents:
        sumdtf = 0
        for w in documents[record]['stemmed']:
            dtf = documents[record]['stemmed'][w]
            sumdtf += math.log10(dtf + 1)
            l = '\t'.join([str(record), w, str(dtf)])
            l += '\n'
            f.write(l)
        l = '\t'.join([str(record), str(sumdtf)])
        l += '\n'
        f1.write(l)
    f.close()


export_U_the_number_of_Unique_terms_in_the_document()
export_plain_dtf_is_the_number_of_times_the_term_appears_in_the_document()
export_stemmed_dtf_is_the_number_of_times_the_term_appears_in_the_document()
