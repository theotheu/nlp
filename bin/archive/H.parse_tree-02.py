import pandas as pd
import nltk
nltk.download("punkt")
nltk.download('averaged_perceptron_tagger')
from nltk.draw.tree import TreeView
from PIL import Image

from nltk import pos_tag, word_tokenize
from collections import Counter
from functools import reduce

nltk.parse.corenlp.CoreNLPParser

from nltk.parse.stanford import StanfordParser, GenericStanfordParser
import os

# CREATE VIRTUAL DISPLAY ###
os.system('Xvfb :1 -screen 0 1600x1200x16  > /dev/null 2>&1  &')    # create virtual display with size 1600x1200 and 16 bit color. Color can be changed to 24 or 8
os.environ['DISPLAY']=':1.0'

# Import stuff for displaying sentence tree

### INSTALL GHOSTSCRIPT (Required to display NLTK trees) ###

from nltk.tree import Tree
from IPython.display import display

## Set environment variables so parser knows where to find parser and models
os.environ['STANFORD_PARSER'] = '/home/theotheu/new_approaches/bin/stanford-parser-full-2020-11-17/stanford-parser.jar'
os.environ['STANFORD_MODELS'] = '/home/theotheu/new_approaches/bin/stanford-parser-4.2.0-models.jar'
os.environ['CLASSPATH'] = '/home/theotheu/new_approaches/bin/stanford-parser-full-2020-11-17/'

parser = StanfordParser(model_path="edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz")

def parseSentence(sentence):
    return parser.raw_parse(sentence)

parsed_sentence = parseSentence("They cannot go to cinema because it is too late.")

def save_img(tree, repos_meta_id=0, sequence=0):
    try:
        t = Tree.fromstring(str(tree))
        filename = str(repos_meta_id) + '-' + str(sequence)

        TreeView(t)._cframe.print_to_file('./output/' + filename + '.ps')

        psimage = Image.open('./output/' + filename + '.ps')
        psimage.load(scale=10)
        psimage.save('./output/' + filename + '.png', format='PNG', quality=100, subsampling=2, dpi=(300, 300))
        # psimage.save('./output/' + filename + '.pdf', format='PDF', resolution=300)
        os.system(' rm -f ./output/' + filename + '.ps')
    except Exception as ex:
        template = "\n\n\nERROR {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print("Error with: ", repos_meta_id, sequence)
        print(message)
        print(tree)


for line in parsed_sentence:
    tree = Tree.fromstring(str(line))
    save_img(tree)

    subtexts = []
    for subtree in tree.subtrees():
        if subtree.label()=="SBAR":
            #print(subtree.label())
            #print(subtree.leaves())
            subtexts.append([' '.join(subtree.leaves()), subtree.label()])

    for text in subtexts:
        print(text)


