# https://www.tensorflow.org/tutorials/load_data/text

import re
import os
from slugify import slugify
import mysql.connector
import dbconfig as cfg

# create db
def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            auth_plugin='mysql_native_password',
            autocommit=True
        )
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            autocommit=True
        )
    return mydb


def get_records(search_terms_id, limit):
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)

    query = 'select m.`comment` '
    query += 'from repos_meta m '
    query += ', search_results r '
    query += ', search_terms t '
    query += 'where (t.id=%s or t.search_terms_id=%s) '
    query += 'and m.id =r.repos_meta_id '
    query += 'and t.id=r.search_terms_id '
    query += 'order by m.id '
    query += 'limit %s '
    data = (search_terms_id, search_terms_id, limit,)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    return myresult


def create_file(out_dir, pattern_name, comments, train_samples):
    file_name_string = slugify(pattern_name)
    # print('>>>> OUT; ', file_name_string)
    

    cmd = "mkdir -p " + out_dir + '/train/' + file_name_string
    stream = os.popen(cmd)
    output = stream.read()

    cmd = "mkdir -p " + out_dir + '/test/' + file_name_string
    stream = os.popen(cmd)
    output = stream.read()

    comment_counter = 0
    for row in comments:
        comment = row[0]
        comment = comment.replace('\n', ' ')
        comment = re.sub("[^a-zA-Z]", " ", comment).split()
        comment = " ".join(comment)

        filename = str(comment_counter).rjust(3, '0')
        if comment_counter < train_samples:
            out_filename = out_dir + '/train/' + file_name_string + '/' + str(comment_counter) + '.txt'
        else:
            out_filename = out_dir + '/test/' + file_name_string + '/' + str(comment_counter) + '.txt'

        f = open(out_filename, 'w')
        f.write('"' + comment + '"\n')
        f.close()

        # print('>>>>>', out_filename)
        comment_counter += 1


def get_categories_and_records(out_dir, limit, train_test_ration):
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)

    cmd = 'rm -fr ./tf/*; mkdir -p tf'
    stream = os.popen(cmd)
    output = stream.read()
    print (output)

    query = 'select id, pattern '
    query += 'from search_terms '
    query += 'where id in ( '
    query += 'select search_terms_id '
    query += 'from search_terms '
    query += 'where search_terms_id is not null '
    query += 'group by search_terms_id)'
    data = ()
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    for row in myresult:
        id = row[0]
        # pattern = row[1].decode('utf-8')
        pattern = row[1]
        print('>>>>> search_terms_id: ', id, pattern)
        comments = get_records(id, limit)
        create_file(out_dir, pattern, comments, limit * train_test_ration)

    mycursor.close()
    mydb.close()


    print ('ALMOST DONE>>>')
    cmd = 'echo "Working dir `pwd`"; #rm -f tf.tar.gz; cd tf; tar -zcvf ../tf.tar.gz .'
    cmd = 'pwd; cd ..; rm -f tf.tar.gz; cd tf; tar -zcvf ../tf.tar.gz .'
    cmd = 'pwd; cd ' + out_dir +'/..; rm -f tf.tar.gz; cd tf; tar -zcvf ../tf.tar.gz .'

    stream = os.popen(cmd)
    output = stream.read()
    print (output)



limit = 2000
train_test_ration = 0.8  #
out_dir = '/Volumes/1tb/tf'
get_categories_and_records(out_dir, limit, train_test_ration)
