import os
import pandas as pd
import glob
import mysql.connector
import csv
import sys
from datetime import datetime
import argparse
import dbconfig as cfg

start_time = datetime.now()
t = datetime.now()


def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        host = cfg.mysql["host"],
        user = cfg.mysql["user"],
        password = cfg.mysql["password"],
        database = cfg.mysql["db"],
        # auth_plugin = 'mysql_native_password'
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"]
        )
    return mydb


def get_comments(mydb):
    startTime = datetime.now()
    mycursor = mydb.cursor()
    query = 'select id, comment from repos_meta where repos_id=8 limit 190000000'
    data = ''
    print('>>>>> query: ', query)
    print('>>>>> data : ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    duration = datetime.now() - startTime
    print("Duration: ", duration)
    return myresult

def save_documents(data):
    my_dir = os.path.dirname(os.path.abspath(__file__))
    target_dir = my_dir + '/corpus/'
    for row in data:
        id = row[0]
        document = row[1]
        filename = target_dir + str(id) + ".txt"
        f = open(filename, "w")
        f.write(document)
        f.close()
        print (id)

mydb = create_db_connection()
data = get_comments(mydb)
save_documents(data)
