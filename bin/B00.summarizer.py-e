# @see https://colab.research.google.com/github/dipanjanS/nlp_workshop_odsc19/blob/master/Module05%20-%20NLP%20Applications/Project06%20-%20Text%20Summarization.ipynb#scrollTo=idmgKnTuFiyg

### 1
import nltk

nltk.download('punkt')
nltk.download('stopwords')
from gensim.summarization import summarize

from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse.linalg import svds
import networkx
from datetime import datetime
import numpy as np
import mysql.connector
import re
import matplotlib.pyplot as plt
import dbconfig as cfg

stop_words = nltk.corpus.stopwords.words('english')

start_time = datetime.now()
t = datetime.now()


def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        host = cfg.mysql["host"],
        user = cfg.mysql["user"],
        password = cfg.mysql["password"],
        database = cfg.mysql["db"],
        # auth_plugin = 'mysql_native_password'
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"]
        )
    return mydb


def get_comments(mydb):
    startTime = datetime.now()
    mycursor = mydb.cursor()
    query = 'select id, comment from repos_meta where length(comment)>1000 limit 19'
    data = ''
    print('>>>>> query: ', query)
    print('>>>>> data : ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    duration = datetime.now() - startTime
    print("Duration: ", duration)
    return myresult


def normalize_document(doc):
    # lower case and remove special characters\whitespaces
    doc = re.sub(r'[^a-zA-Z\s]', '', doc, re.I | re.A)
    doc = doc.lower()
    doc = doc.strip()
    # tokenize document
    tokens = nltk.word_tokenize(doc)
    # filter stopwords out of document
    filtered_tokens = [token for token in tokens if token not in stop_words]
    # re-create document from filtered tokens
    doc = ' '.join(filtered_tokens)
    return doc


def low_rank_svd(matrix, singular_count=2):
    u, s, vt = svds(matrix, k=singular_count)
    return u, s, vt


def get_summary(DOCUMENT):
    ratio = 0.2
    word_count = 100
    print("===================== RATIO:", ratio)
    print(summarize(DOCUMENT, ratio=ratio, split=False))

    ### 5
    print("================ WORD_COUNT:", word_count)
    print(summarize(DOCUMENT, word_count=word_count, split=False))

    sentences = nltk.sent_tokenize(DOCUMENT)
    normalize_corpus = np.vectorize(normalize_document)
    norm_sentences = normalize_corpus(sentences)

    tv = TfidfVectorizer(min_df=0., max_df=1., use_idf=True)
    dt_matrix = tv.fit_transform(norm_sentences)
    dt_matrix = dt_matrix.toarray()

    vocab = tv.get_feature_names_out()
    td_matrix = dt_matrix.T

    num_sentences = 8
    num_topics = 3

    try:
        u, s, vt = low_rank_svd(td_matrix, singular_count=num_topics)
    except:
        print("! ERROR")
        return
    print(u.shape, s.shape, vt.shape)
    term_topic_mat, singular_values, topic_document_mat = u, s, vt

    # remove singular values below threshold
    sv_threshold = 0.5
    min_sigma_value = max(singular_values) * sv_threshold
    singular_values[singular_values < min_sigma_value] = 0

    salience_scores = np.sqrt(np.dot(np.square(singular_values),
                                     np.square(topic_document_mat)))
    print(salience_scores)

    top_sentence_indices = (-salience_scores).argsort()[:num_sentences]
    top_sentence_indices.sort()

    print('\n'.join(np.array(sentences)[top_sentence_indices]))

    similarity_matrix = np.matmul(dt_matrix, dt_matrix.T)
    print(similarity_matrix.shape)
    np.round(similarity_matrix, 3)

    similarity_graph = networkx.from_numpy_array(similarity_matrix)

    plt.figure(figsize=(12, 6))
    networkx.draw_networkx(similarity_graph, node_color='lime')
    plt.show(block=True)

    ### 18
    scores = networkx.pagerank(similarity_graph)
    ranked_sentences = sorted(((score, index) for index, score
                               in scores.items()),
                              reverse=True)
    print(ranked_sentences[:10])

    ### 19
    try:
        top_sentence_indices = [ranked_sentences[index][1]
                                for index in range(num_sentences)]
    except:
        print("! 2nd ERROR")
    top_sentence_indices.sort()

    ### 20
    print('\n'.join(np.array(sentences)[top_sentence_indices]))


def process_dataset(data):
    for row in data:
        id = row[0]
        comment = row[1]

        print("\n\n####################################", id)
        print(comment)
        print("==========================================\n")
        get_summary(comment)


mydb = create_db_connection()
data = get_comments(mydb)

process_dataset(data)

########################################################################################################################
