# @see https://colab.research.google.com/github/littlecolumns/ds4j-notebooks/blob/master/text-analysis/notebooks/Introduction%20to%20topic%20modeling.ipynb
### 1

### 2
import pandas as pd
import matplotlib.pyplot as plt

# These styles look nicer than default pandas
plt.style.use('ggplot')

# We'll be able to see more text at once
pd.set_option("display.max_colwidth", 100)

### 3

### 4
#
# pip install PyStemmer
# 
from sklearn.feature_extraction.text import TfidfVectorizer
import Stemmer

# English stemmer from pyStemmer
stemmer = Stemmer.Stemmer('en')

analyzer = TfidfVectorizer().build_analyzer()

### 5

### 6
from sklearn.decomposition import NMF

### 7

### 8

### 9

### 10

### 11

### 12

### 13
# speeches = pd.read_csv("data/state-of-the-union.csv")
speeches = pd.read_csv("data/git-angular-comments.csv")
print (speeches.sample(5))

### 14
print (speeches.shape)

### 15
# Remove non-word characters, so numbers and ___ etc
speeches.content = speeches.content.str.replace("[^A-Za-z ]", " ")
print (speeches.head())

### 16
from sklearn.feature_extraction.text import TfidfVectorizer
import Stemmer

# English stemmer from pyStemmer
stemmer = Stemmer.Stemmer('en')

analyzer = TfidfVectorizer().build_analyzer()

# Override TfidfVectorizer
class StemmedTfidfVectorizer(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(TfidfVectorizer, self).build_analyzer()
        return lambda doc: stemmer.stemWords(analyzer(doc))


### 17
vectorizer = StemmedTfidfVectorizer(stop_words='english')
matrix = vectorizer.fit_transform(speeches.content)

words_df = pd.DataFrame(matrix.toarray(),
                        columns=vectorizer.get_feature_names_out())
words_df.head()

### 18
model = NMF(n_components=15) #15
model.fit(matrix)

n_words = 10 #10
feature_names = vectorizer.get_feature_names_out()

topic_list = []
for topic_idx, topic in enumerate(model.components_):
    top_n = [feature_names[i]
             for i in topic.argsort()
             [-n_words:]][::-1]
    top_features = ' '.join(top_n)
    topic_list.append(f"topic_{'_'.join(top_n[:3])}")

    print(f"Topic {topic_idx}: {top_features}")

### 19
vectorizer = StemmedTfidfVectorizer(stop_words='english', min_df=5, max_df=0.5)
matrix = vectorizer.fit_transform(speeches.content)

words_df = pd.DataFrame(matrix.toarray(),
                        columns=vectorizer.get_feature_names_out())
print ('#19 ', words_df.head())

for row in words_df:
    print (row)

### 20
model = NMF(n_components=15) # 15
model.fit(matrix)

n_words = 10 #10
feature_names = vectorizer.get_feature_names_out()

topic_list = []
for topic_idx, topic in enumerate(model.components_):
    top_n = [feature_names[i]
             for i in topic.argsort()
             [-n_words:]][::-1]
    top_features = ' '.join(top_n)
    topic_list.append(f"TOPIC_{'_'.join(top_n[:3])}")

    print(f"TOPIC {topic_idx}: {top_features}")

### 21
# Convert our counts into numbers
amounts = model.transform(matrix) * 100

# Set it up as a dataframe
topics = pd.DataFrame(amounts, columns=topic_list)
print (topics.head(2))
for row in topics:
    print (row)

### 22
ax = topics.sum().to_frame().T.plot(kind='barh', stacked=True)
#plt.show(block=True)
# Move the legend off of the chart
ax.legend(loc=(1.04,0))

### 23
x_axis = speeches.year
y_axis = topics

fig, ax = plt.subplots(figsize=(10,5))

# Plot a stackplot - https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/stackplot_demo.html
ax.stackplot(x_axis, y_axis.T, baseline='wiggle', labels=y_axis.columns)
#plt.show(block=True)
# Move the legend off of the chart
ax.legend(loc=(1.04,0))

### 24
merged = topics.join(speeches)

# ax = merged.plot(x='year', y=['topic_kansa_slave_slaveri', 'topic_soviet_communist_atom'], figsize=(10,3))
ax = merged.plot(x='year', y=['TOPIC_fix_error_anim', 'TOPIC_releas_cut_note'], figsize=(10,3))
ax.legend(loc=(1.04,0))
# plt.show(block=True)