import argparse
import csv
import os
import sys
import re
from datetime import datetime

import mysql.connector
import nltk
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import sent_tokenize, word_tokenize

import dbconfig as cfg

nltk.download('wordnet')
nltk.download('stopwords')
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()
csv.field_size_limit(sys.maxsize)
start_time = datetime.now()

Nt_plain_dict = {}
Nt_stemmed_dict = {}
FT_plain_dict = {}
FT_stemmed_dict = {}
number_of_documents = 0
plain_words_per_document = {}

start_time = datetime.now()
minimal_word_length = 2

# Defining paths
parser = argparse.ArgumentParser()
parser.add_argument('--repository', '-r', help="Name of the repository", type=str, default='.')

args = parser.parse_args()

repo_name = args.repository

BIN_DIRECTORY = os.getcwd()
parts = BIN_DIRECTORY.split('/')
parts.pop()
BASE_DIR = '/'.join(parts)
SOURCE_DIR = BASE_DIR + '/repositories/' + repo_name
print('SOURCE_DIR   :', SOURCE_DIR)
ANALYSIS_DIR = BASE_DIR + '/analysis/' + repo_name
print('ANALYSIS_DIR :', ANALYSIS_DIR)

meta_repos_dict = {}


def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            auth_plugin='mysql_native_password',
            autocommit=True
        )
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            auth_plugin='mysql_native_password',
            autocommit=True
        )
    return mydb


def get_search_terms():
    print('\n>>>>> Start reading search terms')
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)
    query = 'select id, pattern, stemmed,lemmatized from search_terms where is_active = 1'
    data = ()
    print('>>>>> query: ', query)
    print('>>>>> data: ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    print('>>>>> Length of list ' + str('{:,.0f}'.format(len(myresult))))
    return myresult


def stemSearchTerm(sentence):
    stem_sentence=[]
    lemmatize_sentence=[]

    sanitized_words = re.sub("[^a-zA-Z]", " ", sentence).split()
    for w in sanitized_words:
        stem_sentence.append(stemmer.stem(w))
        # stem_sentence.append(" ")

        lemmatize_sentence.append(wordnet_lemmatizer.lemmatize(w))
        # lemmatize_sentence.append(" ")

    return [stem_sentence, lemmatize_sentence]


def process_records(records):
    print('Processing records')
    mydb = create_db_connection()
    mycursor = mydb.cursor(buffered=True)

    for record in records:
        id = record[0]
        w = record[1]


        p = stemSearchTerm(w)
        sw = ' '.join(p[0])
        lw = ' '.join(p[1])

        query = 'update search_terms set '
        query += ' stemmed=%s,'
        query += ' lemmatized=%s,'
        query += ' modification_date=now() '
        query += 'where id=%s'
        data = (sw,lw, id, )

        try:
            mycursor.execute(query, data)
            mydb.commit()
        except mysql.connector.Error as err:
            print("Something went wrong: {}".format(err))
            print('meta_repos_id: ', id, query, data)


search_terms = get_search_terms()
#
# search_terms = [
#     [0, 'multi-criteria requirement prioritization'],
#     [1, 'do-178c'],
#     [2, 'agent oriented software engineering'],
#     [3, 'oo (object-oriented)']
#
# ]
#
# # print (type(search_terms));
# # exit();


process_records(search_terms)

print('\n>>>>> number of documents: ', '{:,.0f}'.format(number_of_documents))
print('>>>>> unique plain words for all documents: ', '{:,.0f}'.format(len(Nt_plain_dict)))
print('>>>>> unique stemmed words for all documents: ', '{:,.0f}'.format(len(Nt_stemmed_dict)))
print('>>>>> duration: ', start_time.now() - start_time, start_time.now())

print('>>>>> DONE! ', start_time.now() - start_time, start_time.now())
