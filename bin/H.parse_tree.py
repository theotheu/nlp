import mysql.connector
import dbconfig as cfg
import nltk
from nltk import pos_tag

nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
import hashlib
from nltk.tree import Tree
from nltk.draw.tree import TreeView
from PIL import Image
import os
import pandas as pd
from nltk import RegexpParser


def create_db_connection():
    if cfg.mysql["host"] == 5.7:
        host = cfg.mysql["host"],
        user = cfg.mysql["user"],
        password = cfg.mysql["password"],
        database = cfg.mysql["db"],
        # auth_plugin = 'mysql_native_password'
    else:
        mydb = mysql.connector.connect(
            host=cfg.mysql["host"],
            user=cfg.mysql["user"],
            password=cfg.mysql["password"],
            database=cfg.mysql["db"],
            buffered=True
        )
    return mydb


def get_keywords(mydb):
    keywords = []
    mycursor = mydb.cursor()
    query = """
select t.pattern
from search_terms t
where  t.relevance>%s
order by t.pattern
        """
    data = (.8,)
    print('>>>>> query: ', query)
    print('>>>>> data : ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()

    for r in myresult:
        keywords.append(r[0])

    return keywords


def get_data(mydb):
    mycursor = mydb.cursor()
    query = """
select count(1) cnt, c.id repos_meta_id, c.subject, c.comment
from search_terms t
, search_results r
, repos_meta c
where 
	r.repos_id>%s
and length(c.comment)<2000
and t.relevance>%s
and t.id=r.search_terms_id
and c.id=r.repos_meta_id
group by c.id
limit %s
    """
    data = (22, 0.8, 500000,)
    print('>>>>> query: ', query)
    print('>>>>> data : ', data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    mycursor.close()
    return myresult


def clean_up_comment(subject, comment):
    comment = comment.replace(subject, '', 1).strip()
    comment = comment.replace('\n', ' ')
    sentences = comment.split('. ')
    return (sentences)


def parse_tree_____(data):
    trees = []
    keyword_list = get_keywords(mydb)

    patterns = """mychunk:{<NN.?>*<VBD.?>*<JJ.?>*<CC>?}"""

    for r in data:
        repos_meta_id = r[1]
        subject = r[2]
        comment = r[3]

        cleaned_comments = clean_up_comment(subject, comment)

        for comment in (cleaned_comments):
            comment = comment.replace('\n', ' ')
            sentences = comment.split('. ')

            sentence_counter = 0
            for sentence in sentences:
                if any(word in sentence for word in keyword_list):
                    # text = sentence.split()
                    tokens = nltk.tokenize.word_tokenize(sentence)
                    tags = pos_tag(tokens)
                    entities = nltk.chunk.ne_chunk(tags)
                    print(entities)
                    trees.append([repos_meta_id, sentence_counter, entities])
                    sentence_counter += 1
    return (trees)


def save_tree(mycursor, cause_or_effect, tree, repos_meta_id, search_terms_id):
    tree = ','.join(tree)
    md5 = hashlib.md5(tree.encode('utf-8')).hexdigest()
    query = """
           select `id` from trees where `md5`=%s and cause_or_effect=%s
           """
    data = (md5, cause_or_effect,)
    # print(">>>>> query: ", query)
    # print(">>>>> data: ", data)
    mycursor.execute(query, data)
    myresult = mycursor.fetchall()
    count = mycursor.rowcount
    # print(">>>>> count: ", count)
    if count == 0:
        # Create tree record
        query = "insert ignore into trees (md5, tree, cause_or_effect, creation_date) values (%s, %s, %s, now())"
        data = (md5, tree, cause_or_effect)
        mycursor.execute(query, data)
        mydb.commit()
        trees_id = mycursor.lastrowid
    else:
        trees_id = myresult[0][0]

    # create intersection comments_with_trees


    query = """
            select `id` from comments_with_trees where repos_meta_id=%s and trees_id=%s
            """
    data = (repos_meta_id, trees_id,)
    # print(">>>>> query: ", query)
    # print(">>>>> data: ", data)
    mycursor.execute(query, data)
    count = mycursor.rowcount
    # print(">>>>> count: ", count)
    if count == 0:
        # Create tree record
        query = "insert ignore into comments_with_trees (repos_meta_id, trees_id, creation_date) values (%s, %s, now())"
        data = (repos_meta_id, trees_id,)
        mycursor.execute(query, data)
        mydb.commit()


def parse_tree(mydb, data):
    mycursor = mydb.cursor()
    trees = []
    causes = []
    effects = []
    keyword_list = get_keywords(mydb)

    for r in data:
        repos_meta_id = r[1]
        subject = r[2]
        comment = r[3]

        cleaned_comments = clean_up_comment(subject, comment)

        for comment in (cleaned_comments):
            comment = comment.replace('\n', ' ')
            sentences = comment.split('. ')

            sentence_counter = 0
            for sentence in sentences:
                for k in keyword_list:
                    if k in sentence:
                        parts = sentence.split(k)
                        part_counter = 0
                        cause = []
                        effect = []
                        for part in parts:
                            tokens = nltk.tokenize.word_tokenize(part)
                            if len(tokens) > 0:
                                tags = pos_tag(tokens)
                                try:
                                    mywords, mytags = zip(*tags)
                                    if not mytags or len(mytags) == 0:
                                        print('--- mytags is an empty tuple')
                                        mytags = [('<NONE>')]
                                    if part_counter == 0:
                                        effects.append(mytags)
                                        save_tree(mycursor, 'E', mytags, repos_meta_id, 1)
                                    else:
                                        causes.append(mytags)
                                        save_tree(mycursor, 'C', mytags, repos_meta_id, 1)
                                    entities = nltk.chunk.ne_chunk(tags)
                                    trees.append([repos_meta_id, sentence_counter, entities])
                                    sentence_counter += 1
                                    part_counter += 1
                                except Exception as ex:
                                    template = "\n\n\nERROR {0} occurred. Arguments:\n{1!r}"
                                    message = template.format(type(ex).__name__, ex.args)
                                    print("Error with    : ", repos_meta_id)
                                    print('ERROR MESSAGE : ', message)
                                    print('        PART : ', part)
                                    print('      TOKENS : ', tokens)
                                    print('         TAGS : ', tags)

    # causes = pd.DataFrame({'causes': causes})
    # effects = pd.DataFrame({'effects': effects})
    #
    # print('Cause parse trees')
    # print(causes.groupby(['causes']).size().reset_index(name='count').sort_values(['count'], ascending=False).head(20))
    #
    # print('Effect parse trees')
    # print(
    #     effects.groupby(['effects']).size().reset_index(name='count').sort_values(['count'], ascending=False).head(20))

    mydb.close()
    return (trees, causes, effects)


def save(mydb, results):
    mycursor = mydb.cursor()

    # trees = results[0]
    causes = results[1]
    effects = results[2]

    print('===========')
    print(results)
    print(results[:5])

    print('=====> CAUSES', causes, '<=====')
    for r in causes:
        repos_meta_id = r[0]
        sequense = r[1]
        tree = r[2]
        print('TREE r', r)
        print('TREE cause', tree)
        md5 = hashlib.md5(tree.encode('utf-8')).hexdigest()

        # check if tree exist
        query = """
        select `id` from trees where `md5`=%s
        """
        data = (md5,)
        print(">>>>> query: ", query)
        print(">>>>> data: ", data)
        mycursor.execute(query, data)
        myresult = mycursor.fetchall()
        count = mycursor.rowcount
        print(">>>>> count: ", count)
        if count == 0:
            # Create tree record
            query = "insert into trees (md5, tree, cause_or_effect, creation_date) values (%s, %s, %s, now())"
            data = (md5, tree, 'C')
            mycursor.execute(query, data)
            mydb.commit()

        # if tree exist, check if
    mycursor.close()


def draw(trees):
    os.system(
        'Xvfb :1 -screen 0 1600x1200x16 > /dev/null 2>&1  & ')  # create virtual display with size 1600x1200 and 16 bit color. Color can be changed to 24 or 8
    os.environ['DISPLAY'] = ':1.0'

    for r in trees:
        repos_meta_id = r[0]
        sequense = r[1]
        tree = r[2]
        # print (tree)
        try:
            t = Tree.fromstring(str(tree))
            filename = str(repos_meta_id) + '-' + str(sequense)

            TreeView(t)._cframe.print_to_file('./output/' + filename + '.ps')

            psimage = Image.open('./output/' + filename + '.ps')
            psimage.load(scale=10)
            psimage.save('./output/' + filename + '.png', format='PNG', quality=100, subsampling=2, dpi=(300, 300))
            # psimage.save('./output/' + filename + '.pdf', format='PDF', resolution=300)
            os.system(' rm -f ./output/' + filename + '.ps')
        except Exception as ex:
            template = "\n\n\nERROR {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print("Error with: ", repos_meta_id, sequense)
            print(message)
            print(tree)


def print_explanation():
    print("""
    CC: Coordinating conjunction
    CD: Cardinal number
    DT: Determiner
    EX: Existential there
    FW: Foreign word
    IN: Preposition or subordinating conjunction
    JJ: Adjective
    VP: Verb Phrase
    JJR: Adjective, comparative
    JJS: Adjective, superlative
    LS: List item marker
    MD: Modal
    NN: Noun, singular or mass
    NNS: Noun, plural
    PP: Preposition Phrase
    NNP: Proper noun, singular Phrase
    NNPS: Proper noun, plural
    PDT: Pre determiner
    POS: Possessive ending
    PRP: Personal pronoun Phrase
    PRP: Possessive pronoun Phrase
    RB: Adverb
    RBR: Adverb, comparative
    RBS: Adverb, superlative
    RP: Particle
    S: Simple declarative clause
    SBAR: Clause introduced by a (possibly empty) subordinating conjunction
    SBARQ: Direct question introduced by a wh-word or a wh-phrase.
    SINV: Inverted declarative sentence, i.e. one in which the subject follows the tensed verb or modal.
    SQ: Inverted yes/no question, or main clause of a wh-question, following the wh-phrase in SBARQ.
    SYM: Symbol
    VBD: Verb, past tense
    VBG: Verb, gerund or present participle
    VBN: Verb, past participle
    VBP: Verb, non-3rd person singular present
    VBZ: Verb, 3rd person singular present
    WDT: Wh-determiner
    WP: Wh-pronoun
    WP: Possessive wh-pronoun
    WRB: Wh-adver""")


mydb = create_db_connection()
data = get_data(mydb)
results = parse_tree(mydb, data)
# draw(trees)
# save(mydb, results)

print("Done!")
