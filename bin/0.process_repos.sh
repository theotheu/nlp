#!/bin/bash

unameOut="$(uname -s)"
case "${unameOut}" in
Linux*) PYTHON_VERSION=python3 ;;
Darwin*) PYTHON_VERSION=python ;;
*) machine="UNKNOWN:${unameOut}" ;;
esac

echo "Running on ${unameOut} with python=${PYTHON_VERSION}"

# https://unix.stackexchange.com/questions/129391/passing-named-arguments-to-shell-scripts
while [ $# -gt 0 ]; do
  case "$1" in
  -r | -repo | --repo)
    REPO="$2"
    ;;
  -s | -steps | --steps)
    STEPS="$2"
    ;;
  *)
    printf "***************************\n"
    printf "* Error: Invalid argument.*\n"
    printf "***************************\n"
    exit 1
    ;;
  esac
  shift
  shift
done

echo "USAGE:"
echo "-r <repository>"
echo "   example: -r bash"
echo "   <repository> must exist"
echo "-s <steps>"
echo "   example: -s 1,2,30,31,4,5,6,A0,A1,A2,A3"
echo "   <steps> separated by a comma, without a space"
echo



IFS=',' read -r -a array <<< "${STEPS}"


export LC_ALL=C

export BASE_DIRECTORY=$(pwd)/..
export OLDPWD=$(pwd)

export WORKING_DIRECTORY="${BASE_DIRECTORY}/repositories"
cd $WORKING_DIRECTORY
WORKING_DIRECTORY=$(pwd)
echo "Working directory: $WORKING_DIRECTORY"

process_all_repos() {
  for i in $(ls -d */); do
    DIR=${i%%/}
    process_single_repo $DIR
   done
}

step_1() {
  ./1.clone_repos.sh
}

step_2() {
  ./2.cloneResetRepos.sh
}
step_30() {
  SCRIPT="3.0.getCommitMessagesFromRepositories.sh"
  LOG_FILE="_${1}.3.0.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.30.out\""
  rm -fr $LOG_FILE
  ./${SCRIPT} -r $1 > $LOG_FILE
}

step_31() {
  SCRIPT="3.1.importLogsIntoMysql.py"
  LOG_FILE="_${1}.3.1.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.31.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

step_4() {
  SCRIPT="4.add_branch_to_merged.py"
  LOG_FILE="_${1}.4.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.A1.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

step_5() {
  SCRIPT="5.0.create_documents.py"
  LOG_FILE="_${1}.5.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.5.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

step_6() {
  SCRIPT="6.calculate_tf-idf.py"
  LOG_FILE="_${1}.6.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.6.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

step_A0() {
  SCRIPT="A0.search_terms_stemmer_and_lemmatizer.py"
  LOG_FILE="_${1}.A1.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.A1.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

step_A1() {
  SCRIPT="A1.search_mem.py"
  LOG_FILE="_${1}.A1.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.A1.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

step_A2() {
  SCRIPT="A2.calculate_tf-idf.py"
  LOG_FILE="_${1}.A2.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.A2.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

step_A3() {
  SCRIPT="A3.import_search_results_into_database.py"
  LOG_FILE="_${1}.A3.out"
  echo "------------------------------------------------------------------------"
  echo "Running ${SCRIPT} for $1  LOG_FILE=\"_${1}.A3.out\""
  rm -fr $LOG_FILE
  $PYTHON_VERSION -u ${SCRIPT} -r $1 > $LOG_FILE
}

process_single_repo() {
  echo "================================================================"
  REPO_DIR="${WORKING_DIRECTORY}/${1}"
  REPO_DIR=$OLDPWD
  cd $REPO_DIR
  echo "Processing: $1"
  echo "WORKING_DIRECTORY: ${REPO_DIR}"
  echo "See the \"_${1}.<bash|python-file>.out\" files for the output"

  for STEP in "${array[@]}"
  do
    if [[ $STEP == "1" ]]
    then
      step_1
    elif [[ $STEP == "2" ]]
    then
      step_2
    elif [[ $STEP == "30" ]]
    then
      step_30 $1
    elif [[ $STEP == "31" ]]
    then
      step_31 $1
    elif [[ $STEP == "4" ]]
    then
      step_4 $1
    elif [[ $STEP == "5" ]]
    then
      step_5 $1
    elif [[ $STEP == "6" ]]
    then
      step_4 $1
    elif [[ $STEP == "A0" ]]
    then
      step_A0 $1
    elif [[ $STEP == "A1" ]]
    then
      step_A1 $1
    elif [[ $STEP == "A2" ]]
    then
      step_A2 $1
    elif [[ $STEP == "A3" ]]
    then
      step_A3 $1
    else
      echo "!!! Undefined: $STEP"
    fi
  done

  cd $OLDPWD
}



if [ "$REPO" != '' ]; then
  process_single_repo $REPO
else
  process_all_repos
fi
